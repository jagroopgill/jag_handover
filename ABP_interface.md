# ABP Interface

Logon to the server using the details on the wiki and remote desktop to any of the following servers

The three main servers are:

abprepdev1 - database server

10.208.220.134 - test interface with QAS and pre-prod environments

abp-app73 - live interface known as prod

The easiest way files can be transferred to the server is by using a dropbox account.

## 10.208.220.134

### QAS

Files are located here: 
C:\msvr_interface\interface

Files get created here: \\abp-fsv11\output$\QAS\System\Mainsaver\HUM\To-Process

And once picked up and successfully processed are moved to here: \\abp-fsv11\output$\QAS\System\Mainsaver\HUM\Archive

Database parameters: C:\msvr_interface\interface\pdi_files\parameters

### Pre-Prod

Files are located:
C:\msvr_interface\interface_preprod

Files get created here: \\abp-fsv11\output$\PREPROD\System\Mainsaver\HUM\To-Process

And once picked up and successfully processed are moved to here: \\abp-fsv11\output$\PREPROD\System\Mainsaver\HUM\Archive

Database parameters can be found here: C:\msvr_interface\interface_preprod\pdi_files\parameters

## abp-app73

Files are located:
C:\msvr_interface\interface_prod

Files get created here: \\abp-fsv11\PRODOutput$\Prod\System\Mainsaver\HUM\To-Process

And once picked up and successfully processed are moved to here: \\abp-fsv11\PRODOutput$\Prod\System\Mainsaver\HUM\Archive

Database parameters can be found here: C:\msvr_interface\interface_prod\pdi_files\parameters

## Jobs that are running:

### po_export 

There is a trigger which triggers when PO's are added they appear in a staging table with an action 'A'.  They are picked up from here by the interface to then be exported to an xml file.

### pochange_export

There is a trigger which triggers when PO's are edited they appear in a staging table with an action 'E'.  They are picked up from here by the interface to then be exported to an xml file.

### gr_export

This was orginally written by Gary.  The files are still on QAS, Pre-prod and Prod servers, but the jobs are disabled and no longer running.  This was still kept as a backup should the re-written goods receipt cause any issues.

### gr_export_new

ABP requested that there old goods receipt interface was re-written.  So a new one was written and implemented and is currently running with no issues (Feb 2020) on their live server.  The files are also on QAS and pre-prod which are used for testing.

The interface uses 2 tables and a view: 
All good receipts are inserted into SPX_PORD_DETAILS when triggered.  
The interface then uses a view called SPX_PORD_VIEW which retrieves all the records from SPX_PORD_DETAILS, which are older than 10 minutes and does not have an entry in SPX_PORD_PROCESSED (i.e. has not been processed).  
Records that have been successfully processed has an entry on their uniq_id stored in SPX_PORD_PROCESSED.

There is a stored procedure on their database which allows the user to insert directly into polled table called spx_ins_goods_receipt which they requested if they had to manually insert a PO.

### gr_export_purge

This job deletes records older than a month from the spx_pord_details and spx_pord_processed tables for the gr_export_new job.  It is currently running in QAS and I need to confirm that this can be applied to live (Feb 2020).

### stck_journal_export

This is run nightly which etrieves the 'MT41' and 'MT51' transactions after a certain date from STXNS and exports them to a file.

### invoice

This retrieves information from an XML file and populates the APIV and APIVD tables accordingly.



