# AMG Interface 

The two parts of the interface that was written by myself were:

PO and STXNS Export

## Triggers and tables required for setup
A trigger has been created called <b>spx_stxns_trig.sql</b> which exports all lines of a PO at the point of first receipt.  The details get exported to the <b>spx_pupo_table</b>.  
It also exports the details of the line that has been receipted to the <b>spx_stxns_polled</b> table.

## poExport folder

The interface generally uses the same files that are used for the other jobs, except for the following files and scripts:

create_pupo_tables.sql
create_spx_stxns_polled.sql
create_spx_stxns_trig.sql

poExport.kjb - (The main job) This checks whether PO rows need processing.  If so, process them and then the receipts.  If no PO lines are to be processed, continue to check whether any stxns rows are to be processed.  If so, continue to process.

headerDetailexport.kjb - This send the PO number that is currently being processed to...

poHeaderExport.ktr
This gets all the header information and then the detail information for a specific PO and inserts the header information into mainsaver_order first, and then all the detail lines after.

stxnsExport.kjb - This job runs if there are receipts to be processed. It gets the stxnsID (which just a unique ID number for each line in the spx_stxns_polled table) and using this retrieves the relevant row from the table to be inserted.
Once it is inserted the row is deleted from the polled table.

At the end of this process it executes the Oracle Scheduler Job to start the import process.

## Invoice Import

Matched invoices will be imported into Mainsaver with a status of 'Clear'.

The following procedures are required for this import:

spx_upd_apiv_invno_counter - This creates the unique number for the invoice number in Mainsaver (apiv_invno)
spx_upd_apiv_counter - This creates a uniquie number for inv_id for the import.
spx_get_stcabu_period - this works out what budget period the invoice is part of.

invImport.kjb - (The main job) This checks the Oracle database table to see whether any rows need to be processed.  It calls...

getInvoices.ktr - which gets the all the invoice numbers so they can be sent one at a time through the interface to be processed.

importTables.kjb - this calls all the transformations required for the import:
apivImp.ktr - import all the apiv header lines
apivdImp.ktr - import all the apivd header lines
insTotals.ktr - this totals up all the values of the detail lines to populate the total fields in the header table e.g. line_item_total, inv_total_cost etc.

When all the imports are complete it updates the processed flag in the Oracle database to say that it has been processed.

## Few key points about the current files: 

* Please check the fields on the import transformations - especially as this hasn't been tested in their environment.

* The PO number is an Integer field in the Oracle database whereas Mainsaver accepts a String.  So to develop the interface a constant was added 'U' which is appended to the order_no coming across (Calculator step) so the correct PO was used to retrieve information. This will highly likely to have to be changed.  This is in apivImp.ktr and apivdImp.ktr

* The SQL selects in the job currently look for rows where the processed_flag is null in mainsaver_invoice.  As we haven't tested this using their data there may be a chance that this may need to be changed.